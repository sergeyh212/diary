@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Подтвердите свою электронную почту</div>

                    <div class="card-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                Новая ссылка для подтверждения была отправлена вам на элетронную почту.
                            </div>
                        @endif

                        Прежде чем продолжить, проверьте свою почту, чтобы получить ссылку для подтверждения.
                        Если вы не получили письмо,
                        <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                            @csrf
                            <button type="submit" class="btn btn-link p-0 m-0 align-baseline">нажмите сюда, чтобы получить
                                еще письмо</button>.
                        </form>
                    </div>
                </div>
                {{-- <a href="{{ route('loverif') }}" class="btn btn-primary mt-2">Назад</a> --}}

            </div>
        </div>
    </div>
@endsection
