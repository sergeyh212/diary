@extends('layouts.main')

@section('content')
    <form action="{{ route('note.store') }}" method="post">
        <div class="container text-center">
            @csrf

            <div class="mb-4">
                <h4>Заголовок</h4>
                <input name="title" id="title" value="{{ old('title') }}">
                @error('title')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Текст</h4>
                <textarea name="edited_text" id="myeditorinstance">{{ old('edited_text') }}</textarea>
                @error('edited_text')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Дата</h4>
                <input name="created_at" id="created_at" type="datetime-local" value="{{ date('Y-m-d H:i') }}"
                    min="2015-01-01T00:00" max="2099-12-31T23:59">
                @error('created_at')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Уведомить до события за (мин)</h4>
                <span>Вкл/выкл</span>
                <input type="checkbox" name="switch" >
                <input name="notification_time" id="notification_time" type="number" min="1"
                    value="{{ old('notification_time') ? old('notification_time') : 1 }}">
                @error('notification_time')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Категория</h4>
                <select name="category_id">
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
                @error('category_id')
                    <p>{{ $message }}</p>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
