@extends('layouts.main')

@section('content')
    <div class="fixed text-center">
        <input type="text" name="name" id="name" value="{{ isset($_GET['name']) ? $_GET['name'] : '' }}">
        <select name="category" id="category">
            <option value="all">Все категории</option>
            @foreach ($categories as $category)
                <option value="{{ $category->id }}"
                    @if (isset($_GET['category'])) {{ $_GET['category'] == $category->id ? 'selected' : '' }} @endif>
                    {{ $category->name }}
                </option>
            @endforeach
        </select>
        <select name="time" id="time">
            <option value="1" @if (isset($_GET['time'])) {{ $_GET['time'] == 1 ? 'selected' : '' }} @endif>
                Все время</option>
            <option value="2" @if (isset($_GET['time'])) {{ $_GET['time'] == 2 ? 'selected' : '' }} @endif>
                Сегодня</option>
            <option value="3" @if (isset($_GET['time'])) {{ $_GET['time'] == 3 ? 'selected' : '' }} @endif>
                Неделя</option>
            <option value="4" @if (isset($_GET['time'])) {{ $_GET['time'] == 4 ? 'selected' : '' }} @endif>Месяц
            </option>
        </select>

        <button class="btn btn-dark search">Поиск</button>

        <a class="btn btn-secondary" href="{{ route('note.index') }}">Сбросить</a>
    </div>
    {{-- <div class="text-center mt-5">
        @if (count($notes) == 0)
            <p>По вашему запросу ничего не найдено!</p>
        @endif
    </div> --}}

    <div class=" container">
        <div class="box-container card-font">
            @foreach ($notes as $note)
                <div class="box" id="{{ $note->category->color_id }}" name="card">
                    <h3>{{ $note->title }}</h3>
                    <p class="">{{ $note->created_at }}</p>
                    <p class="text-truncate">{{ $note->text }}</p>
                    <p class="card-text ">
                        <a class="text-dark text-decoration-none" href="{{ route('category.show', $note->category) }}">
                            @if ($note->category_id != 1)
                                {{ $note->category->name }}
                            @endif
                        </a>
                    </p>
                    <a href="{{ route('note.show', $note) }}" class="card-link text-decoration-none">Посмотреть</a>
                </div>
            @endforeach
        </div>


        <div class="pagin">{{ $notes->links('pagination::bootstrap-4') }}</div>

        <div class="btn-add-note end-0">
            <a href="{{ route('note.create') }}" class="btn btn-primary">Добавить заметку</a>
        </div>

        <script src="{{ asset('js/request.js') }}"></script>
        <script src="{{ asset('js/color.js') }}"></script>
    @endsection
