@extends('layouts.main')

@section('content')
    <div class="container text-center">
        <h3>{{ $note->title }}</h3>

        <div class="mb-4">
            <p>{{ $note->created_at }}</p>
        </div>

        <div class="mb-4">
            <p>{{ $note->category->name }}</p>
        </div>

        <script type="text/javascript">
            tinymce.init({
                selector: "textarea",
                readonly: true,
                toolbar: false,
                menubar: false,
            });
        </script>
        <div class="mb-4">
            <textarea class="form-control" id="textarea">{{ $note->edited_text }}</textarea>
        </div>

        <div>
            <a class="btn btn-warning mb-4" href="{{ route('file.index', $note) }}">Файлы</a>
        </div>

        <div>
            <a class="btn btn-primary mb-4" href="{{ route('note.edit', $note) }}">Изменить</a>
        </div>

        <div>
            <form action="{{ route('note.destroy', $note) }}" method="post">
                @method('delete')
                @csrf
                <button class="btn btn-danger">Удалить</button>
            </form>
        </div>
    </div>
@endsection
