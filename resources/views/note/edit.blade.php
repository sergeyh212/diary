@extends('layouts.main')

@section('content')
    <form action="{{ route('note.update', $note) }}" method="post">
        <div class="container text-center">
            @method('patch')
            @csrf

            <h3>Редактирование заметки</h3>
            <hr>

            <div class="mb-4">
                <h4>Заголовок</h4>
                <input name="title" id="title" value="{{ $note->title }}">
                @error('title')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Текст</h4>
                <textarea name="edited_text" id="myeditorinstance" class="form-control">{{ $note->edited_text }}</textarea>
                @error('text')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Дата</h4>
                <input name="created_at" id="created_at" type="datetime-local" value="{{ $note->created_at }}"
                    min="2015-01-01T00:00" max="2099-12-31T23:59">
                @error('created_at')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>
            <div class="mb-4">
                <h4>Уведомить до события за (мин)</h4>
                <span>Вкл/выкл</span>
                <input type="checkbox" name="switch" {{ $note->switch == 'on' ? 'checked' : '' }}>
                <input name="notification_time" id="notification_time" type="number" min="1"
                    value="{{ $note->notification_time }}">
                @error('notification_time')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Категория</h4>
                <select name="category_id">
                    @foreach ($categories as $category)
                        <option {{ $category->id === $note->category_id ? 'selected' : '' }} value="{{ $category->id }}">
                            {{ $category->name }}</option>
                    @endforeach
                </select>
                @error('category_id')
                    <p>{{ $message }}</p>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
