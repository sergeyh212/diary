@extends('layouts.main')

@section('content')
    <form action="{{ route('file.index', $note) }}" method="post" enctype="multipart/form-data">

        <div class="container text-center">
            @csrf

            <div class="mb-4">
                <h4>Название файла</h4>
                <input name="name" id="name" value="{{ old('name') }}">
                @error('name')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <input class="text-center" type="file" name="file">
                @error('file')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
    </form>
@endsection
