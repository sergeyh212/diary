@extends('layouts.main')

@section('content')
    <div class="container">
        <h3 class="text-center">Список файлов</h3>
        <table class="table ">
            <thead class="table-dark">
                <tr>
                    <td>Название файла</td>
                    <td></td>
                    <td>Действие</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($files as $file)
                    <tr>
                        <td>{{ $file->name }}</td>
                        <td>
                            <img style="height: 100px" src="{{asset('assets/'.$file->file)}}" alt="">
                        </td>
                        <td>
                            <a class="btn btn-info mb-1" href="{{ route('file.download', [$note, $file]) }}">Скачать</a>
                            <form action="{{ route('file.destroy', [$note, $file]) }}" method="post">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div>
        <a href="{{ route('file.create', $note) }}" class="btn btn-primary btn-edit-category end-0">Добавить файл</a>
    </div>
@endsection
