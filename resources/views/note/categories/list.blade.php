@extends('layouts.main')

@section('content')
    <div class="container">
        <h3 class="text-center">Список категорий</h3>
        <table class="table ">
            <thead class="table-dark">
                <tr>
                    <td>Название</td>
                    <td>Действие</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                    <tr class="">
                        <td>{{ $category->name }}</td>
                        <td class="">
                            <a class="btn btn-primary mb-1" href="{{ route('category.edit', $category) }}">Изменить</a>
                                <form action="{{ route('category.destroy', $category) }}" method="post">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Удалить</button>
                                </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
