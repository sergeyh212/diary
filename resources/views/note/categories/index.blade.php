@extends('layouts.main')

@section('content')
   
    <h3 class="text-center mb-5">Категории</h3>

    <div class="container text-center">
        @foreach ($categories as $category)
            <div class=" mb-3">
                <a class="btn btn-light" href="{{ route('category.show',  $category) }}">{{ $category->name }}</a>
            </div>
        @endforeach
    </div>

    <div >
        <a href="{{ route('category.create') }}" class="btn btn-primary btn-add-category end-0">Добавить категории</a>
        <a href="{{ route('category.list') }}" class="btn btn-secondary btn-edit-category end-0">Настроить категории</a>
    </div>

@endsection
