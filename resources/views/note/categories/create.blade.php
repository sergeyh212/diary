@extends('layouts.main')

@section('content')
    <form action="{{ route('category.store') }}" method="post">

        <div class="container text-center">
            @csrf

            <div class="mb-4">
                <h4>Название категории</h4>
                <input name="name" id="name" value="{{ old('name') }}">
                @error('name')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Цвет</h4>
                <select name="color_id" id="">
                    @foreach ($colors as $color)
                        <option value="{{ $color->id }}">{{ $color->name }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
    </form>
@endsection
