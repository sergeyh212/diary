@extends('layouts.main')

@section('content')
    <form action="{{ route('category.update', $category) }}" method="post">
        @method('patch')
        @csrf

        <div class="container text-center">

            <div class="mb-4">
                <h4>Название категории</h4>
                <input name="name" id="name" value="{{ $category->name }}">
                @error('name')
                    <span class="text-danger" role="alert">
                        <p>{{ $message }}</p>
                    </span>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Цвет</h4>
                <select name="color_id" id="">
                    @foreach ($colors as $color)
                        <option value="{{ $color->id }}" {{ $color->id === $category->color_id ? 'selected' : '' }}>
                            {{ $color->name }}
                        </option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
