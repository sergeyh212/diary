@extends('layouts.main')


@section('content')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('css/about.css') }}">
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-col">
                    <h4>Адрес</h4>
                    <ul>
                        <li>
                            <p>РЕСПУБЛИКА БЕЛАРУСЬ</p>
                        </li>
                        <li>
                            <p>г. Витебск, ул Гагарина 41,</p>
                        </li>
                        <li>
                            <p>210017</p>
                        </li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>Контакты</h4>
                    <ul>
                        <li>Почта<a style="text-transform: lowercase" href="mailto:sergeyh212@gmail.com">
                                sergeyh212@gmail.com</a></li>
                        <li>Телефон<a href="tel:+375336553153"> +375336553153</a></li>
                        <li>Факс<p> 36-17-08 </p>
                        </li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>Сайт</h4>
                    <ul>
                        <li> <a style="text-transform: lowercase" href="https://vitgtk.belstu.by/">
                                vitgtk.belstu.by
                            </a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>Соц. сети</h4>
                    <ul>
                        <div class="social-links">
                            <a href="https://vk.com/v_i_t_g_t_k"><i class="fab fa-vk"></i></a>
                            <a href="https://www.instagram.com/vitgtk.belstu_official/"><i class="fab fa-instagram"></i></a>
                        </div>
                    </ul>
                </div>

            </div>
        </div>
        <h4 class="text-center mt-3">Гляд С.К. 2023</h4>
    </footer>
@endsection
