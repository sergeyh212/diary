<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
   <link rel="stylesheet" href="{{asset('css/card.css')}}">
    <script src="{{asset('tinymce/tinymce.min.js')}}"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea#myeditorinstance', // Replace this CSS selector to match the placeholder element for TinyMCE
            menubar: false,
            plugins: 'lists', // add image
            toolbar: 'undo redo | blocks | bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code', //add image
        });
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @vite(['resources/sass/app.scss', 'resources/js/app.js'])

    <title>Dairy</title>
</head>

<body>

    <nav class="nb fixed-top">
        <a href="{{ route('check') }}" class="logo text-decoration-none">Diary</a>
        <div class="nav-links">
            <ul>
                @if (Auth::user()->id == 1)
                    <li><a class="text-decoration-none" href="{{ route('admin.index') }}">Пользователи</a></li>
                @else
                    <li> <a class="text-decoration-none" href="{{ route('note.index') }}">Заметки</a></li>
                    <li><a class="text-decoration-none" href="{{ route('category.index') }}">Категории</a></li>
                    <li> <a class="text-decoration-none" href="{{ route('user.show', [auth()->user()]) }}">Личный
                            кабинет
                        </a></li>
                    <li><a href="{{ route('note.about') }}" class="text-decoration-none">О нас</a></li>
                @endif
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-end logout" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item text-center" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            Выйти
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        <img src="{{ asset('images/menu-btn.png') }}" alt="menu hamburger" class="menu-hamburger">
    </nav>
    <header>
    </header>

    @yield('content')

</body>
<script src="{{ asset('/suneditor/suneditor.min.js') }}"></script>

<script>
    var menuHamburger = document.querySelector(".menu-hamburger")
    var navLinks = document.querySelector(".nav-links")

    menuHamburger.addEventListener('click', () => {
        navLinks.classList.toggle('mobile-menu')
    })
</script>

</html>
