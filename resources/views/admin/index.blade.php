@extends('layouts.main')

@section('content')
    <div class="container">
        <h3 class="text-center">Список пользователей</h3>
        <table class="table ">
            <thead class="table-dark">
                <tr>
                    <td>ID</td>
                    <td>Логин</td>
                    <td>Email</td>
                    <td>Действие</td>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td class="text-break">{{ $user->login }}</td>
                        <td class="text-break">{{ $user->email }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ route('admin.show', $user) }}">Посмотреть</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
