@extends('layouts.main')

@section('content')
    <form action="{{ route('admin.update', $user) }}" method="post">
        <div class="container text-center">
            @method('patch')
            @csrf

            <h3>Редактирование пользователя</h3>
            <hr>

            <div class="mb-4">
                <h4>Имя</h4>
                <input name="name" value="{{ $user->name }}">
                @error('name')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Логин</h4>
                <input name="login" value="{{ $user->login }}">
                @error('login')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Email</h4>
                <input name="email" value="{{ $user->email }}">
                @error('email')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
    </form>
@endsection
