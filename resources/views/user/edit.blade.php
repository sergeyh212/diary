@extends('layouts.main')

@section('content')
    <div class="container text-center">

        <form action="{{ route('user.update', $user) }}" method="post">
            @method('patch')
            @csrf

            <h3>Редактирование данных</h3>
            <hr>

            <div class="mb-4">
                <h4>Имя</h4>
                <input name="name" value="{{ $user->name }}">
                @error('name')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>

            <div class="mb-4">
                <h4>Логин</h4>
                <input name="login" value="{{ $user->login }}">
                @error('login')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
            </div>



            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
        <div class=" mt-4">
            <a class="btn btn-secondary mb-3" href="{{ route('email', auth()->user()) }}">Сменить email</a>
        </div>
        <div class="mb-4">
            <form action="{{ route('password.email') }}" method="POST">
                @csrf
                <input type="text" name="email" value="{{ auth()->user()->email }}" hidden>
                <button type="submit" class="btn btn-secondary">Сменить пароль</button>
            </form>
        </div>
    </div>
@endsection
