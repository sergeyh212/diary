@extends('layouts.main')

@section('content')
    <div class="container text-center">
        <h3>Личный кабинет</h3>
        <hr>

        <div class="mb-4">
            <h4>Имя</h4>
            <p>{{ $user->name }}</p>
        </div>

        <div class="mb-4">
            <h4>Логин</h4>
            <p>{{ $user->login }}</p>
        </div>

        <div class="mb-4">
            <h4>Email</h4>
            <p>{{ $user->email }}</p>
        </div>

        <div>
            <a class="btn btn-primary mb-4" href="{{ route('user.edit', $user) }}">Изменить данные</a>
        </div>

        <div class="text-end">
            <form action="{{ route('user.destroy', $user) }}" method="post">
                @method('delete')
                @csrf
                <button class="btn btn-danger ">Удалить аккаунт</button>
            </form>
        </div>

    </div>
@endsection
