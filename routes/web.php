<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\EmailController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Note\NoteController;
use App\Http\Controllers\Note\CategoryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [HomeController::class, 'main'])->name('main')->middleware('mainPages');
Route::get('/404', [HomeController::class, 'error'])->name('error.404');
Route::get('/loverif', [HomeController::class, 'loverif'])->name('loverif');

Route::group([
    'prefix' => 'admin',
    'middleware' => ['admin', 'verified']
], function () {
    Route::get('/users', [AdminController::class, 'index'])->name('admin.index');
    Route::get('/users/{user}', [AdminController::class, 'show'])->name('admin.show');
    Route::get('/users/{user}/edit', [AdminController::class, 'edit'])->name('admin.edit');
    Route::patch('/users/{user}', [AdminController::class, 'update'])->name('admin.update');
    Route::delete('/users/{user}', [AdminController::class, 'destroy'])->name('admin.destroy');
});

Route::group(
    [
        'prefix' => 'user',
        'middleware' => ['user', 'verified'],
    ],
    function () {
        Route::get('/email/{user}', [EmailController::class, 'email'])->name('email');
        Route::post('/email/{user}', [EmailController::class, 'changeEmail'])->name('changeEmail');

        Route::get('/{user}', [UserController::class, 'show'])->name('user.show');
        Route::get('/{user}/edit', [UserController::class, 'edit'])->name('user.edit');
        Route::patch('/{user}', [UserController::class, 'update'])->name('user.update');
        Route::delete('/{user}', [UserController::class, 'destroy'])->name('user.destroy');
    }
);


Route::group([
    'prefix' => 'notes',
    'middleware' => ['note', 'verified']
], function () {
    Route::get('/', [NoteController::class, 'index'])->name('note.index');
    Route::post('/', [NoteController::class, 'index'])->name('note.index');
    Route::get('/create', [NoteController::class, 'create'])->name('note.create');
    Route::post('/', [NoteController::class, 'store'])->name('note.store');
    Route::get('/about', [NoteController::class, 'about'])->name('note.about');


    Route::get('{note}/files/create', [FileController::class, 'create'])->name('file.create');
    Route::post('{note}/files', [FileController::class, 'store'])->name('file.store');

    Route::group(['middleware' => 'file'], function () {
        Route::get('/{note}/files', [FileController::class, 'index'])->name('file.index');
        Route::get('/{note}/files/{file}', [FileController::class, 'download'])->name('file.download');
        Route::delete('{note}/files/{file}', [FileController::class, 'destroy'])->name('file.destroy');
    });

    Route::group(
        ['middleware' => 'note.checkNote'],
        function () {
            Route::get('/{note}', [NoteController::class, 'show'])->name('note.show');
            Route::get('/edit/{note}', [NoteController::class, 'edit'])->name('note.edit');
            Route::patch('/{note}', [NoteController::class, 'update'])->name('note.update');
            Route::delete('/{note}', [NoteController::class, 'destroy'])->name('note.destroy');
        }
    );
});


Route::group([
    'prefix' => 'categories',
    'middleware' => ['note', 'verified']

], function () {
    Route::get('/', [CategoryController::class, 'index'])->name('category.index');
    Route::get('/create', [CategoryController::class, 'create'])->name('category.create');
    Route::post('/', [CategoryController::class, 'store'])->name('category.store');
    Route::get('/list', [CategoryController::class, 'list'])->name('category.list');

    Route::group([
        'middleware' => 'category.checkCategory'
    ], function () {
        Route::get('/{category}', [CategoryController::class, 'show'])->name('category.show');
        Route::get('/edit/{category}', [CategoryController::class, 'edit'])->name('category.edit');
        Route::patch('/{category}', [CategoryController::class, 'update'])->name('category.update');
        Route::delete('/{category}', [CategoryController::class, 'destroy'])->name('category.destroy');
    });
});

Auth::routes(['verify' => true]);

Route::get('/check', [HomeController::class, 'login'])->name('check');
