<?php


namespace App\Services\Category;

use App\Models\Note;

class CategoryService
{
    public function __construct()
    {
        date_default_timezone_set('Europe/Minsk');
    }

    public function show($request, $category)
    {
        $notes = $this->search($request, $category);

        return $notes;
    }

    private function search($request, $category)
    {
        $notes = Note::query();

        $notes->where('user_id', auth()->user()->id)
            ->where('category_id', $category->id)->get();

        if ($request->filled('name')) {
            $notes->where('title', 'like', "%{$request->name}%");
        }

        if ($request->filled('category') && $request->category != 'all') {
            $notes->where('category_id', $request->category);
        }

        $notes->orderBy('id', 'desc');

        $notes = $notes->paginate(12)->withQueryString();

        return $notes;
    }
}