<?php


namespace App\Services\Note;

use App\Jobs\QueueSenderEmail;
use App\Mail\Note\SendMail;
use App\Models\Note;
use DateTime;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\ValidationException;

class NoteService
{
    public function __construct()
    {
        date_default_timezone_set('Europe/Minsk');
    }

    public function index($request)
    {
        $notes = $this->search($request);

        return $notes;
    }

    public function store($data)
    {

        if (strtotime(now()) >= strtotime($data['created_at']) && isset($data['switch'])) {
            $error = ValidationException::withMessages([
                'notification_time' => 'Дата указана в прошедшем времени, для отправки уведомления укажите в будущем!'
            ]);

            throw $error;
        }

        $data['user_id'] = auth()->user()->id;

        $data['created_at'] = str_replace('T', ' ', $data['created_at']);

        $note = Note::firstOrCreate($data);
        if (isset($data['switch']) && $data['created_at'] > now()) {
            $data['email_id'] = $note->id;

            $this->sendMail($data);
        }
    }

    public function update($data, $note)
    {

        if (strtotime(now()) >= strtotime($data['created_at']) && isset($data['switch'])) {
            $error = ValidationException::withMessages([
                'notification_time' => 'Дата указана в прошедшем времени, для отправки уведомления укажите в будущем!'
            ]);

            throw $error;
        }

        $data['user_id'] = auth()->user()->id;

        $data['created_at'] = str_replace('T', ' ', $data['created_at']);

        $data['email_id'] = $note->id;

        if ($data['notification_time'] != $note->notification_time || $data['created_at'] != $note->created_at) {

            if (DB::table('jobs')->where('payload', 'like', '%email_id\\\\"' . ";i:{$note->id}%")->first() != null) {
                DB::table('jobs')->where('payload', 'like', '%email_id\\\\"' . ";i:{$note->id}%")->delete();
            }

            if (isset($data['switch']) && $data['created_at'] > now()) {
                $this->sendMail($data);
            }
        }

        if (!isset($data['switch'])) {
            $data['switch'] = null;
        }

        unset($data['email_id']);

        $note->update($data);
    }

    public function destroy($note)
    {
        if (DB::table('jobs')->where('payload', 'like', '%email_id\\\\"' . ";i:{$note->id}%")->first() != null) {
            DB::table('jobs')->where('payload', 'like', '%email_id\\\\"' . ";i:{$note->id}%")->delete();
        }
    }

    private function search($request)
    {
        $notes = Note::query();

        $notes->where('user_id', auth()->user()->id);

        if ($request->filled('name')) {
            $notes->where(function ($query) use ($request) {
                $query->where('title', 'like', "%{$request->name}%")
                    ->orWhere('text', 'like', "%{$request->name}%");
            });
        }

        if ($request->filled('category') && $request->category != 'all') {
            $notes->where('category_id', $request->category);
        }

        if ($request->filled('time') && $request->time != 1) {
            if ($request->time == 2) {
                $notes->whereBetween('created_at', [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59', strtotime(today()))]);
            }

            if ($request->time == 3) {
                $notes->whereBetween('created_at', [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59', strtotime('+7 days'))]);
            }

            if ($request->time == 4) {
                $notes->whereBetween('created_at', [date('Y-m-d 00:00:00'), date('Y-m-d 23:59:59', strtotime('+30 days'))]);
            }
        }

        $notes->orderBy('id', 'desc');

        $notes = $notes->paginate(12)->withQueryString();

        return $notes;
    }

    private function sendMail($data)
    {
        $time = strtotime($data['created_at']) - strtotime(now()) - $data['notification_time'] * 60;

        Mail::to(auth()->user()->email)->later($time, new SendMail($data));
    }
}