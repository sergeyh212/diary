<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = $request->user;

        if (
            auth()->user() == null ||
            auth()->user()->id != $user->id ||
            auth()->user()->id == 1
        ) {
            return redirect()->route('error.404');
        }

        return $next($request);
    }
}