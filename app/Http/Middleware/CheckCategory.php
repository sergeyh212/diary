<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CheckCategory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $category = $request->category;

        if (
            auth()->user()->id != $category->user_id && $category->user_id != 1
        ) {
            return redirect()->route('error.404');
        }

        return $next($request);
    }
}
