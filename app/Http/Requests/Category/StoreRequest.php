<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:2|max:20',
            'color_id' => ''
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Поле обязательно для заполнеия!',
            'name.min' => 'Название должно состоять минимум из 2 символов!',
            'name.max' => 'Название должно состоять максимум из 20 символов!'
        ];
    }
}