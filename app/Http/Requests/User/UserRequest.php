<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:255'],
            'login' => ['required', 'string', 'min:3', 'max:255', "unique:users,login,{$this->user->id}"],
            'email' => ['required', 'string', 'email', 'max:255', "unique:users,email,{$this->user->id}"]
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Поле обязательно для заполнения!',
            'login.required' => 'Поле обязательно для заполнения!',
            'name.min' => 'Имя должно состоять минимум из 3 символов!',
            'login.min' => 'Логин должен состоять минимум из 3 символов!',
            'name.max' => 'Имя должно состоять максимум из 255 символов!',
            'login.max' => 'Логин должен состоять максимум из 255 символов!',
            'email.max' => 'Email должен состоять максимум из 255 символов!',
            'login.unique' => 'Логин уже занят!',
            'email.unique' => 'Email уже занят!',
        ];
    }
}