<?php

namespace App\Http\Requests\Note;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'min:3', 'max:25'],
            'edited_text' => ['required', 'string', 'min:3', 'max:5000'],
            'created_at' => ['required'],
            'category_id' => '',
            'notification_time' => ['required', 'numeric', 'min:0', 'max:720'],
            'switch' => '',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Поле обязательно для заполнения!',
            'edited_text.required' => 'Поле обязательно для заполнения!',
            'notification_time.required' => 'Поле обязательно для заполнения!',
            'title.min' => 'Заголовок должен состоять минимум из 3 символов!',
            'edited_text.min' => 'Текст должен состоять минимум из 3 символов!',
            'title.max' => 'Заголовок должен состоять максимум из 25 символов!',
            'edited_text.max' => 'Текст должен состоять максимум из 5000 символов!',
            'notification_time.max' => 'Максимальное количество минут: :max!',
            'created_at.required' => 'Поле обязательно для заполнения!',
        ];
    }
}