<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/email/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make(
            $data,
            [
                'name' => ['required', 'string', 'min:3', 'max:255'],
                'login' => ['required', 'string', 'min:3', 'max:255', 'unique:users'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ],
            [

                'name.required' => 'Поле обязательно для заполнения!',
                'login.required' => 'Поле обязательно для заполнения!',
                'password.required' => 'Поле обязательно для заполнения!',
                'password.confirmed' => 'Пароли не совпадают!',
                'name.min' => 'Имя должно состоять минимум из 3 символов!',
                'login.min' => 'Логин должен состоять минимум из 3 символов!',
                'password.min' => 'Пароль должен состоять минимум из 8 символов',
                'name.max' => 'Имя должно состоять максимум из 255 символов!',
                'login.max' => 'Логин должен состоять максимум из 255 символов!',
                'email.max' => 'Email должен состоять максимум из 255 символов!',
                'login.unique' => 'Логин уже занят!',
                'email.unique' => 'Email уже занят!',
            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'login' => $data['login'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

    }


}
