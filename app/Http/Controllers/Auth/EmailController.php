<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\EmailRequest;
use App\Models\Category;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class EmailController extends Controller
{

    public function email(User $user)
    {

        return view('auth.email.email');
    }

    public function changeEmail(EmailRequest $request, User $user)
    {
        $data = $request->validated();

        $data['email_verified_at'] = null;

        $user->update($data);

        event(new Registered($user));

        session()->flash('status', 'Ссылка успешно отправлена па почту!');

        return $request->wantsJson()
            ? new JsonResponse([], 201)
            : redirect()->route('email', auth()->user());
    }


}