<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\File\FileService;

class BaseFileController extends Controller
{
    public $service;

    public function __construct(FileService $service)
    {
        $this->service = $service;
    }
}