<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UserRequest;
use App\Models\Category;
use App\Models\File;
use App\Models\Note;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserRequest $request, User $user)
    {
        $data = $request->validated();

        $user->update($data);

        return redirect()->route('user.show', compact('user'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $notes =  Note::where('user_id', $user->id)->get();
        $categories = Category::where('user_id', $user->id)->get();
        $files = File::where('user_id', $user->id)->get();

        foreach ($files as $file) {
            $file->delete();
        }

        foreach ($notes as $note) {
            $note->delete();
        }

        foreach ($categories as $category) {
            $category->delete();
        }

        $user->delete();

        return redirect()->route('login');
    }
}