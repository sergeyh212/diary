<?php

namespace App\Http\Controllers;

use App\Http\Requests\File\StoreRequest;
use App\Models\File;
use App\Models\Note;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class FileController extends BaseFileController
{
    public function index(Note $note)
    {
        $files = File::where('user_id', auth()->user()->id)
            ->where('note_id', $note->id)
            ->get();


        return view('note.files.index', compact('files', 'note'));
    }

    public function create(Note $note)
    {
        return view('note.files.create', compact('note'));
    }

    public function store(StoreRequest $request, Note $note)
    {
        $data = $request->validated();

        $file = $request->file('file');

        if (
            (count(File::where('name', $file->getClientOriginalName())
                ->where('note_id', $note->id)
                ->get()) != 0 && !isset($data['name'])) ||
            count(File::where('name', $data['name'] . '.' . $file->getClientOriginalExtension())
                ->where('note_id', $note->id)
                ->get()) != 0

        ) {
            $error = ValidationException::withMessages([
                'file' => 'Файл с таким именем уже существует!'
            ]);

            throw $error;
        }

        $data['user_id'] = auth()->user()->id;
        $data['note_id'] = $note->id;

        if (isset($data['name'])) {
            $data['name'] = $data['name'] . '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('files' . $note->id, $data['name']);
        } else {
            $data['name'] = $file->getClientOriginalName();
            $path = $file->storeAs('files' . $note->id, $file->getClientOriginalName());
        }

        $data['file'] = $path;

        File::create($data);

        return redirect()->route('file.index', $note);
    }

    public function download(Note $note, File $file)
    {
        $path = '/var/www/public/assets/files' . $note->id . '/' . $file->name;

        if (file_exists($path)) {
            return Response::download($path);
        }
    }

    public function destroy(Note $note, File $file)
    {
        Storage::delete($file->file);
        $file->delete();

        return redirect()->route('file.index', $note);
    }
}