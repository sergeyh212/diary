<?php

namespace App\Http\Controllers;

use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function main()
    {
        return view('main');
    }


    public function login()
    {
        $users = User::where('id', '>', 1)->get();

        if (auth()->user() != null) {
            if (auth()->user()->id == 1) {
                return redirect()->route('admin.index', compact('users'));
            } else {
                return redirect()->route('note.index');
            }
        } else {
            return redirect()->route('main');
        }
    }

    public function error()
    {
        return view('errors.404');
    }

    public function loverif()
    {
        setcookie('XSRF-TOKEN', '', time() - 999999999999, '/');

        setcookie('laravel_session', '', time() - 999999999999, '/');

        return redirect()->route('main');
    }
}