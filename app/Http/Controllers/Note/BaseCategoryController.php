<?php


namespace App\Http\Controllers\Note;

use App\Http\Controllers\Controller;
use App\Services\Category\CategoryService;

class BaseCategoryController extends Controller
{
    public $service;

    public function __construct(CategoryService $service)
    {
        $this->service = $service;
    }
}