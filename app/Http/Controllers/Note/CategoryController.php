<?php

namespace App\Http\Controllers\Note;

use App\Http\Requests\Category\StoreRequest;
use App\Http\Requests\Category\UpdateRequest;
use App\Models\Category;
use App\Models\Color;
use App\Models\Note;
use Illuminate\Http\Request;

class CategoryController extends BaseCategoryController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categories = Category::where('user_id', auth()->user()->id)
            ->orWhere('user_id', 1)
            ->get();

        return view('note.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $colors = Color::all();

        return view('note.categories.create', compact('colors'));
    }

    public function list()
    {
        $categories = Category::where('user_id', auth()->user()->id)
            ->orWhere('user_id', 1)
            ->where('id', '>', 1)
            ->get();

        return view('note.categories.list', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        $data['user_id'] = auth()->user()->id;

        Category::create($data);

        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, Category $category)
    {

        $notes =  $this->service->show($request, $category);

        $categories = Category::all();


        return view('note.categories.show', compact('notes', 'categories', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        $colors = Color::all();

        return view('note.categories.edit', compact('category', 'colors'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Category $category)
    {
        $data = $request->validated();

        $category->update($data);

        return redirect()->route('category.list');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        $notes = Note::where('category_id', $category->id)->get();

        foreach ($notes as $note) {
            $note->update(['category_id' => 1]);
        }

        $category->delete();

        return redirect()->route('category.list');
    }
}