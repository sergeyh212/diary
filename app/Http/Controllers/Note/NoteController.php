<?php

namespace App\Http\Controllers\Note;

use App\Http\Requests\Note\UpdateRequest;
use App\Http\Requests\Note\StoreRequest;
use App\Models\Category;
use App\Models\Note;
use Illuminate\Http\Request;

class NoteController extends BaseNoteController
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $notes =  $this->service->index($request);
        $categories = Category::where('user_id', auth()->user()->id)
            ->orWhere('user_id', 1)
            ->get();

        return view('note.index', compact('notes', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $categories = Category::where('user_id', auth()->user()->id)
            ->orWhere('user_id', 1)
            ->get();

        return view('note.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();

        $data['text'] = strip_tags($request['edited_text']);

        $this->service->store($data);

        return redirect()->route('note.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Note $note)
    {
        return view('note.show', compact('note'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Note $note)
    {
        $categories = Category::where('user_id', auth()->user()->id)
            ->orWhere('user_id', 1)
            ->get();

        return view('note.edit', compact('note', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Note $note)
    {
        $data = $request->validated();

        $data['text'] = strip_tags($request['edited_text']);

        $this->service->update($data, $note);

        return redirect()->route('note.show', $note);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Note $note)
    {
        $note->delete();

        $this->service->destroy($note);

        return redirect()->route('note.index');
    }

    public function about()
    {
        return view('note.about');
    }
}