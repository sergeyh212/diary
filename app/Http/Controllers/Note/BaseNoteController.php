<?php


namespace App\Http\Controllers\Note;

use App\Http\Controllers\Controller;
use App\Services\Note\NoteService;

class BaseNoteController extends Controller
{
    public $service;

    public function __construct(NoteService $service)
    {
        $this->service = $service;
    }
}