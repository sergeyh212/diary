$(document).ready(function () {
    $('.search').click(function (e) {
        e.preventDefault();
        let name = document.getElementById('name').value;
        let category = document.getElementById('category').value;
        let time = document.getElementById('time').value;

        $.ajax({
            url: "/notes",
            type: "GET",
            data: {
                name: name,
                category: category,
                time: time
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('body').html(response);
                console.log(response)
            },
            error: function (data) {
                console.log('Data error');
            }
        });
    });
});
