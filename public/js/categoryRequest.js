$(document).ready(function () {
    $('.search').click(function (e) {
        e.preventDefault();
        let name = document.getElementById('name').value;

        $.ajax({
            url: window.location,
            type: "GET",
            data: {
                name: name,
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('body').html(response);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });
});
