var colors = {
    1: 'white',
    2: 'red',
    3: 'blue',
    4: 'green',
    5: 'yellow',
    6: 'purple',
    7: 'orange'
};
var keys = Object.keys(colors)
$(document).ready(function () {

    var cards = document.getElementsByName('card');
    cards.forEach(card => {
        keys.forEach(key => {
            if (card.id == key) {
                card.classList.add(colors[key]);
            }
        });
    })

});
